#!/bin/bash
#===================================================================================
#
# FILE: create-d8-composer
#
# USAGE: create-d8-composer [-n <site name>]
#
# DESCRIPTION: This script will create a new vanilla Drupal 7 site.
#
# OPTIONS: see function ’usage’ below
# NOTES: ---
# AUTHOR: Kevin Bailey, kevin.bailey@agilisys.co.uk
# COMPANY: Agilisys
#===================================================================================

##set -u
##set -x
##trap read debug

# -------------------------------------------------------------------------------------------------
# Set up global variables.
#
# We are going to explicitly set the script name variable.  Obviously the script name is available
# as $0 - but this is not consistent and may or may not contain the path.  Also, this means we can
# use the bash debugger without needing to handle the fact that $0 would contain something like
# 'bashdb'.
# -------------------------------------------------------------------------------------------------
SCRIPT_NAME=create-d8-composer
DATE_TIME=$(date +%Y%m%d-%H%M%S)

# Work out the URL of the vhost.
VHOST_URL=${HOSTNAME}.`dnsdomainname`

# -------------------------------------------------------------------------------------------------
# Set up the user to be able to run sudo commands - this will be revoked at the end of the script.
# -------------------------------------------------------------------------------------------------
sudo -v

# -------------------------------------------------------------------------------------------------
# Local functions.
# -------------------------------------------------------------------------------------------------

# Output a usage message - used when the parameters input are not correct.
usage () {
    echo "Usage: $SCRIPT_NAME [-n <name>]"
}

# Create a standard MySQL database and a user account which is set with all privileges
# to the database.
__create_mysql_database_and_user_account () {

    DATABASE_NAME=${1}
    DATABASE_USER=${2}
    DATABASE_PASSWORD=${3}

    mysqladmin create ${DATABASE_NAME}
    mysql --execute="GRANT ALL ON \`${DATABASE_NAME}\`.* TO '${DATABASE_USER}'@'localhost' IDENTIFIED BY '${DATABASE_PASSWORD}';" ${DATABASE_NAME}
}

# Set up an Apache vhost with configuration from template files.
__set_up_vhost () {

    VHOST_NAME=${1}
    DOC_ROOT=${2}
    ASSIGN_USER_ID=${3}
    VHOST_URL=${4}
    
    # First we need to copy the relevant vhost template file.
    sudo cp /usr/local/share/devdep2/vhost_drupal7.template /etc/apache2/sites-available/${VHOST_NAME}.conf

    # Edit the configuration file.  There may be a better way to do this.
    sudo sed -i "s|documentroot|${DOC_ROOT}|g" /etc/apache2/sites-available/${VHOST_NAME}.conf
    sudo sed -i "s/username/${ASSIGN_USER_ID}/g" /etc/apache2/sites-available/${VHOST_NAME}.conf
    sudo sed -i "s/vhostname/${VHOST_NAME}/g" /etc/apache2/sites-available/${VHOST_NAME}.conf
    sudo sed -i "s/serverfqdn/${VHOST_URL}/g" /etc/apache2/sites-available/${VHOST_NAME}.conf
    sudo sed -i "s/serveradminemail/root@localhost/g" /etc/apache2/sites-available/${VHOST_NAME}.conf

    sudo a2ensite ${VHOST_NAME}
    sudo service apache2 restart
}

# -------------------------------------------------------------------------------------------------
# Testing parameters
# -------------------------------------------------------------------------------------------------
while getopts ":n:" opt; do
    case $opt in
        n)
            SUPPLIED_NAME=$OPTARG
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            usage
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            usage
            exit 1
            ;;
    esac
done

##set -x
##trap read debug

# -------------------------------------------------------------------------------------------------
echo
echo `date`
echo "Creating site name..."
# -------------------------------------------------------------------------------------------------

if [ -n "$SUPPLIED_NAME" ]
then

    if [ ${#SUPPLIED_NAME} -gt 16 ]
    then
	echo "ERROR - The project name length has to be 16 characters or less."
	usage
	exit 1
    fi

    SITE_NAME=$SUPPLIED_NAME
else

    SITE_NAME=$(date +%Y%m%d-%H%M%S)
fi

# Convert to lowercase to make name more MySQL friendly.
SITE_NAME="${SITE_NAME,,}"

# Check if the site name has been used already.
if [ -e /etc/apache2/sites-available/${SITE_NAME}.conf ]
then
    echo "ERROR - The site name ${SITE_NAME} is already used."
    usage
    exit 1
fi

echo "SUPPLIED_NAME:"
echo $SUPPLIED_NAME
echo

echo "SITE_NAME:"
echo $SITE_NAME
echo


# -------------------------------------------------------------------------------------------------
echo
echo `date`
echo "Creating database and user..."
# -------------------------------------------------------------------------------------------------

DB_NAME=${SITE_NAME}
DB_USER=${SITE_NAME}
DB_PASSWORD=`cat /dev/urandom | base64 | tr -d '[:punct:]' | tr -d '[:digit:]' | tr -d '[:upper:]' | cut -c1-10 | head -1`

echo "DB: "${DB_NAME}
echo "USER: "${DB_USER}
echo "PASSWORD: "${DB_PASSWORD}

__create_mysql_database_and_user_account ${DB_NAME} ${DB_USER} ${DB_PASSWORD}



# -------------------------------------------------------------------------------------------------
echo
echo `date`
echo "Creating vhost..."
# -------------------------------------------------------------------------------------------------

mkdir -p ~/sites
SITE_DOCROOT=~/sites/${SITE_NAME}
__set_up_vhost ${SITE_NAME} ${SITE_DOCROOT} ${USER} ${VHOST_URL}


# -------------------------------------------------------------------------------------------------
echo
echo `date`
echo "Download and install vanilla Drupal 8 site..."
# -------------------------------------------------------------------------------------------------

##drush dl drupal-8 --drupal-project-rename=${SITE_NAME} --destination=~/sites
##drush -r ~/sites/${SITE_NAME} site-install --db-url=mysql://${DATABASE_USER}:${DATABASE_PASSWORD}@localhost/${DATABASE_NAME} --yes --root=~/sites/${SITE_NAME}

cd ~
echo "Downloading site code ..."
composer create-project drupal/drupal ${SITE_DOCROOT}

echo "Installing Drupal console ..."
cd ${SITE_DOCROOT}
composer require drupal/console:~1.0 --prefer-dist --optimize-autoloader

echo "Installing Drupal site ..."
vendor/bin/drupal site:install  standard --langcode="en" --db-type="mysql" --db-host="127.0.0.1" --db-name="${DB_NAME}" --db-user="${DB_USER}" --db-pass="${DB_PASSWORD}" --db-port="3306" --site-name="${SITE_NAME}" --site-mail="admin@example.com" --account-name="admin" --account-mail="admin@example.com" --account-pass="admin" --no-interaction

# -------------------------------------------------------------------------------------------------
echo
echo `date`
echo "Getting site up to date and clearing caches..."
# -------------------------------------------------------------------------------------------------

echo "Clearing caches ..."
vendor/bin/drupal cr all
vendor/bin/drupal cr discovery

echo "Updating composer packages ..."
composer update

echo "Clearing caches ..."
vendor/bin/drupal cr all
vendor/bin/drupal cr discovery

##drush -r ~/sites/${SITE_NAME} updb -y
##drush -r ~/sites/${SITE_NAME} cr

# -------------------------------------------------------------------------------------------------
echo
echo `date`
echo "Outputting details..."
# -------------------------------------------------------------------------------------------------

##echo "The new site address is: http://${SITE_NAME}.uat.ucas.com"
echo "The admin password has been reset to 'admin'"
echo "The new site URL is: http://"${SITE_NAME}.${VHOST_URL}

sudo -k

# -------------------------------------------------------------------------------------------------
echo
echo `date`
echo "Finished."
# -------------------------------------------------------------------------------------------------

exit 0

