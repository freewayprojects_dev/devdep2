# README #

These should be helper scripts used as building block tools for more specific Devops requirements.

## What is this repository for? ##

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up? ##

### Summary of set up

### Set up steps

On the devqa server certain steps must be carried out before the scripts in this repo will run.


#### Install sudo

Install sudo and then add the developer accounts the sudo group.

    # apt-get install sudo
    # adduser dev1 sudo

#### Make mysql tools available from the command line

For each developer create a file called ~/.my.cnf

In this file add 

    [client]
    user=root
    password=iTy1I

#### Install phpmyadmin

    # apt-get installphpmyadmin

#### Install composer

This is easy enough.  First download a local copy by following: 

https://getcomposer.org/download/

    $ cd ~
    $ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    $ php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    $ php composer-setup.php
    $ php -r "unlink('composer-setup.php');"


Then move the new composer binary to /usr/local/bin to make it globally available - as root run:

    mv /home/devaccount/composer.phar /usr/local/bin/composer


#### Install the scripts

Now we have all the dependencies installed we can install the scripts and use them.

    # cd /usr/local/share
    # git clone https://bailey86@bitbucket.org/freewayprojects_dev/devdep2.git



* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## User guide ###

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ###

* Repo owner or admin
* Other community or team contact